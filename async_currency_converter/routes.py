import pathlib

from aiohttp import web

from async_currency_converter.main.views import ping, convert_currency, update_database

PROJECT_PATH = pathlib.Path(__file__).parent


def init_routes(app: web.Application) -> None:
    add_routes = app.router.add_routes

    add_routes([
        web.get('/', ping),
        web.get('/convert', convert_currency),
        web.post('/database', update_database),
        ]
    )
