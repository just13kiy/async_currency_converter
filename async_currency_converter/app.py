from pathlib import Path
from typing import Optional, List
from functools import partial

from aiohttp import web
import aioredis

from async_currency_converter.routes import init_routes
from async_currency_converter.utils.common import init_config


path = Path(__file__).parent


async def redis(app: web.Application) -> None:
    '''
    A function that, when the server is started, connects to redis,
    and after stopping it breaks the connection (after yield)
    '''
    config = app['config']['redis']

    create_redis = partial(
        aioredis.create_redis,
        f'redis://{config["host"]}:{config["port"]}'
    )

    sub = await create_redis()
    pub = await create_redis()

    app['redis_sub'] = sub
    app['redis_pub'] = pub
    app['create_redis'] = create_redis

    yield

    app['redis_sub'].close()
    app['redis_pub'].close()

    await app['redis_sub'].wait_closed()
    await app['redis_pub'].wait_closed()


def init_app(config: Optional[List[str]] = None) -> web.Application:
    app = web.Application()

    init_config(app, config=config)
    init_routes(app)

    app.cleanup_ctx.extend([
        redis,
    ])

    return app
