import aioredis
import typing as t

if t.TYPE_CHECKING:
    from functools import partial


class CurrencyConverter:
    def __init__(self, create_redis: 'partial[t.Coroutine[t.Any, t.Any, aioredis.Redis]]'):
        self._redis: aioredis.Redis = create_redis()

    async def _get_redis(self):
        return await self._redis

    async def convert(self, source: str, target: str, amount: float) -> float:
        redis_key = f'{source}-{target}'
        redis = await self._get_redis()

        key_exists = await redis.exists(redis_key)

        if key_exists:
            ratio = await redis.get(redis_key)
            return amount * float(ratio)
        else:
            raise ValueError(f'No such key in Redis: {redis_key}')

    async def update_ratio(self, *update_list: t.Dict):
        redis = await self._get_redis()
        await redis.execute('FLUSHALL')
        await self.partial_update_ratio(*update_list, redis=redis)

    async def partial_update_ratio(self, *update_list: t.Dict, redis: t.Optional[aioredis.Redis] = None):
        redis = redis or await self._get_redis()
        for entry in update_list:
            first = entry['first']
            second = entry['second']
            ratio = entry['ratio']

            first_key = f'{first}-{second}'
            second_key = f'{second}-{first}'
            second_ratio = 1 / ratio

            await redis.set(first_key, ratio)
            await redis.set(second_key, second_ratio)
