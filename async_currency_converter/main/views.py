import json
from typing import Dict

from aiohttp import web
from async_currency_converter.utils.types import with_error, CurrencyConverterValidator, DatabaseValidator
from .currency import CurrencyConverter


async def ping(request: web.Request) -> web.Response:
    return web.json_response(data={'ping': True})


@with_error
async def convert_currency(request: web.Request) -> web.Response:
    validator = CurrencyConverterValidator()
    valid_data = validator.validate_query(request.query)

    converter = CurrencyConverter(request.app['create_redis'])
    result = await converter.convert(**valid_data)

    return web.json_response({'converted': result})


@with_error
async def update_database(request: web.Request) -> web.Response:
    validator = DatabaseValidator()
    query = validator.validate_query(request.query)
    body = validator.validate_body(await request.json())

    converter = CurrencyConverter(request.app['create_redis'])

    is_merging = int(query.get('merge', 0))
    if is_merging == 1:
        await converter.partial_update_ratio(*body)
    else:
        await converter.update_ratio(*body)

    return web.Response(status=204)
