import multidict
import trafaret as t
from typing import Dict, List
from functools import wraps

from aiohttp import web


def with_error(fn):
    """
    This is decorator for wrapping web handlers which need to represent
    errors connected with validation if they exist.
    """

    @wraps(fn)
    async def inner(*args, **kwargs):
        try:
            return await fn(*args, **kwargs)
        except t.DataError as e:
            return web.json_response({
                    'errors': e.as_dict(value=True)
                },
                status=400,
            )

    return inner


class QueryValidatorMixin:
    def validate_query(self, query: multidict.MultiDictProxy) -> Dict:
        valid_data = self._query_schema.check(query)
        return valid_data


class BodyValidatorMixin:
    def validate_body(self, body: Dict) -> List:
        valid_data = self._body_schema.check(body)
        return valid_data


class CurrencyConverterValidator(QueryValidatorMixin):
    def __init__(self):
        self._query_schema = t.Dict({
            t.Key('from', to_name='source'): t.String(allow_blank=False),
            t.Key('to', to_name='target'): t.String(allow_blank=False),
            t.Key('amount'): t.ToFloat,
        })


class DatabaseValidator(QueryValidatorMixin, BodyValidatorMixin):
    def __init__(self):
        self._query_schema = t.Dict({
            t.Key('merge'): t.ToInt,
        })

        self._body_schema = t.List(
            t.Dict({
                t.Key('first_currency', to_name='first'): t.String(allow_blank=False),
                t.Key('second_currency', to_name='second'): t.String(allow_blank=False),
                t.Key('ratio'): t.ToFloat,
            })
        )
