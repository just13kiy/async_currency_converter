# Async Currency Converter (0.2.0)

___
## Overview
Simple microservice for storing currency exchange ratio and performing
conversions between currencies.
Ratios can be updated fully or partially.

Boilerplate powered by [create-aio-app](https://github.com/aio-libs/create-aio-app/)
___

## Requirements
- docker-compose
- python 3.7.4^
___

## Local development
    git clone git@gitlab.com:just13kiy/async_currency_converter.git
    poetry install --dev

All develop settings for application are in the `/config/api.dev.yml`.

### Run
To start the project in develop mode, run the following command:

```
make run
```

or just

```
make
```

For stop work of docker containers use:

```
make stop
```

For clean up work of docker containers use:

```
make clean
```

Interactive work inside container

```
make bash # the command must be running after `make run` 
```

### Help

If needed a list of available commands
```
make help
```

### Linters
To run flake8, run the following command:

```
make lint
```

The all settings connected with a `flake8` you can customize in `setup.cfg`.

### Type checking
To run mypy for type checking run the following command:

```
make mypy
```

The all settings connected with a `mypy` you can customize in `mypy.ini`.
___