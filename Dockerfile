FROM python:3.7

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  POETRY_VERSION=0.12.17

RUN pip3 install "poetry==$POETRY_VERSION"

WORKDIR /app

COPY poetry.lock pyproject.toml /application/

RUN poetry config settings.virtualenvs.create false
RUN poetry install --no-interaction --no-dev

ADD . /app
